package com.pingpong 
{
	import com.pingpong.controller.DataCommands;
	import com.pingpong.controller.MoveBallCommand;
	import com.pingpong.controller.MoveCpuCommand;
	import com.pingpong.controller.RacketMoveCommand;
	import com.pingpong.controller.StartUpCommand;
	import com.pingpong.view.component.BallView;
	import com.pingpong.view.component.Racket;
	import com.pingpong.view.RacketMediator;
	import org.puremvc.as3.interfaces.IFacade;
	import org.puremvc.as3.patterns.facade.Facade;
	import org.puremvc.as3.patterns.observer.Notification;
	
	/**
	 * ...
	 * @author yangicher
	 */
	public class AppFacade extends Facade implements IFacade 
	{
		public static const NAME:String	= 'AppFacade';
		public static const STARTUP:String = NAME + 'StartUp';
		
		public static function getInstance():AppFacade {
			return (instance ? instance : new AppFacade()) as AppFacade;
		}
		override protected function initializeController():void 
		{
			super.initializeController();
			registerCommand(STARTUP, StartUpCommand);
			registerCommand(Racket.GET_DATA, DataCommands);
			registerCommand(Racket.MOUSE_MOVES, RacketMoveCommand);
			registerCommand(BallView.UPDATE, MoveBallCommand);
			
			registerCommand(Racket.UPDATE_CPU_RACKET, MoveCpuCommand);
		}
		override public function sendNotification(notificationName:String, body:Object = null, type:String = null):void 
		{
			//trace('sent Notification ' + notificationName);
			notifyObservers(new Notification(notificationName, body, type));
		}
		public function startUp(stage:Object):void {
			sendNotification(STARTUP, stage);
		}
	}

}