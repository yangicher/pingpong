package com.pingpong.controller 
{
	import com.pingpong.model.CpuProxy;
	import com.pingpong.model.DataProxy;
	import com.pingpong.view.component.BallView;
	import org.puremvc.as3.interfaces.ICommand;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	/**
	 * ...
	 * @author 
	 */
	public class MoveBallCommand extends SimpleCommand implements ICommand
	{
		override public function execute(notification:INotification):void 
		{
			dataProxy.moveBall();
			if (dataProxy.dataVO.ballX <= dataProxy.dataVO.ballRadius / 2) {
				dataProxy.ballXPosition = dataProxy.dataVO.ballRadius / 2;
				dataProxy.changeBallSpeedX();
			} else if (dataProxy.dataVO.ballX >= dataProxy.dataVO.gameWidth - dataProxy.dataVO.ballRadius / 2) {
				dataProxy.ballXPosition = dataProxy.dataVO.gameWidth - dataProxy.dataVO.ballRadius / 2;
				dataProxy.changeBallSpeedX();
			}
			
			if (dataProxy.dataVO.ballY <= dataProxy.dataVO.ballRadius / 2) {
				dataProxy.ballYPosition = dataProxy.dataVO.ballRadius / 2;
				dataProxy.changeBallSpeedY();
			} else if (dataProxy.dataVO.ballY >= dataProxy.dataVO.gameHeight - dataProxy.dataVO.ballRadius / 2) {
				dataProxy.ballYPosition =  dataProxy.dataVO.gameHeight - dataProxy.dataVO.ballRadius / 2;
				dataProxy.changeBallSpeedY();
			}
			
			if (dataProxy.dataVO.ballX >= cpuProxy.cpuVO.racketXposition && 
				dataProxy.dataVO.ballX <= cpuProxy.cpuVO.racketXposition + cpuProxy.cpuVO.racketWidth && 
				dataProxy.dataVO.ballY >= cpuProxy.cpuVO.startY) {
					
				dataProxy.changeBallSpeedY();
			}
			if (dataProxy.dataVO.ballX >= dataProxy.dataVO.racketXposition && 
				dataProxy.dataVO.ballX <= dataProxy.dataVO.racketXposition + dataProxy.dataVO.racketWidth && 
				dataProxy.dataVO.ballY <= dataProxy.dataVO.startY + dataProxy.dataVO.racketHeight) {
				
				dataProxy.changeBallSpeedY();
			}
		}
		
		private function get dataProxy():DataProxy 
		{
			return facade.retrieveProxy(DataProxy.NAME) as DataProxy;
		}
		
		private function get cpuProxy():CpuProxy 
		{
			return facade.retrieveProxy(CpuProxy.NAME) as CpuProxy;
		}
	}

}