package com.pingpong.controller 
{
	import com.pingpong.model.DataProxy;
	import com.pingpong.view.component.Racket;
	import org.puremvc.as3.interfaces.ICommand;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * ...
	 * @author yangicher
	 */
	public class DataCommands extends SimpleCommand implements ICommand 
	{
		
		override public function execute(notification:INotification):void 
		{
			var name:String = notification.getName();
			var body:Object = notification.getBody();
			trace('name ' + name);
			
			switch (name) {
				case Racket.GET_DATA:
					proxy.loadData();
				break;
			}
		}
		
		private function get proxy():DataProxy 
		{
			return facade.retrieveProxy(DataProxy.NAME) as DataProxy;
		}
	}

}