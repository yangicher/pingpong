package com.pingpong.controller 
{
	import com.pingpong.model.DataProxy;
	import org.puremvc.as3.interfaces.ICommand;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	/**
	 * ...
	 * @author 
	 */
	public class RacketMoveCommand extends SimpleCommand implements ICommand
	{
		override public function execute(notification:INotification):void 
		{
			var mouseX:int = notification.getBody() as int;
			if (mouseX <= proxy.dataVO.racketWidth / 2) {
				proxy.racketPosition = proxy.dataVO.racketWidth / 2;
			} else if (mouseX >= proxy.dataVO.gameWidth - proxy.dataVO.racketWidth / 2) {
				proxy.racketPosition = proxy.dataVO.gameWidth - proxy.dataVO.racketWidth / 2;
			}
			
			if (mouseX >= proxy.dataVO.racketWidth / 2 && mouseX <= proxy.dataVO.gameWidth - proxy.dataVO.racketWidth / 2) {
				proxy.racketPosition = mouseX;
			}
		}
		
		private function get proxy():DataProxy 
		{
			return facade.retrieveProxy(DataProxy.NAME) as DataProxy;
		}
	}

}