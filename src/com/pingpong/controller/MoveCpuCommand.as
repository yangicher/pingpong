package com.pingpong.controller 
{
	import com.pingpong.model.CpuProxy;
	import org.puremvc.as3.interfaces.ICommand;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	/**
	 * ...
	 * @author 
	 */
	public class MoveCpuCommand extends SimpleCommand implements ICommand
	{
		override public function execute(notification:INotification):void 
		{
			var ballX:int = notification.getBody() as int;

			if (ballX <= proxy.cpuVO.racketWidth / 2) {
				proxy.racketPosition = proxy.cpuVO.racketWidth / 2;
			} else if (ballX >= proxy.cpuVO.gameWidth - proxy.cpuVO.racketWidth / 2) {
				proxy.racketPosition = proxy.cpuVO.gameWidth - proxy.cpuVO.racketWidth / 2;
			}
			
			if (ballX >= proxy.cpuVO.racketWidth / 2 && ballX <= proxy.cpuVO.gameWidth - proxy.cpuVO.racketWidth / 2) {
				proxy.racketPosition = ballX;
			}
		}
		private function get proxy():CpuProxy 
		{
			return facade.retrieveProxy(CpuProxy.NAME) as CpuProxy;
		}
	}

}