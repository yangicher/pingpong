package com.pingpong.controller 
{
	import com.pingpong.model.CpuProxy;
	import com.pingpong.model.DataProxy;
	import com.pingpong.view.AppMediator;
	import com.pingpong.view.StageMediator;
	import org.puremvc.as3.interfaces.ICommand;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * ...
	 * @author yangicher
	 */
	public class StartUpCommand extends SimpleCommand implements ICommand
	{
		override public function execute(notification:INotification):void 
		{
			facade.registerProxy(new DataProxy());
			facade.registerProxy(new CpuProxy());
			facade.registerMediator(new AppMediator(notification.getBody() as Game));
		}	
	}

}