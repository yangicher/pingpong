package com.pingpong.view.component 
{
	import com.demonsters.debugger.MonsterDebugger;
	import com.pingpong.model.vo.DataVO;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author yangicher
	 */
	public class Racket extends Sprite 
	{
		public static const NAME:String = 'Racket';
		//public static const MOVE:String = NAME + 'Move';
		public static const GET_DATA:String = NAME + 'GetData';
		public static const DATA_READY:String = NAME + 'DataReady';
		public static const CLICKED:String = NAME + 'Clicked';
		
		
		public static const MOUSE_MOVES:String = 'mouseMoves';
		public static const MOVE_RACKET:String = 'moveRacket';
		
		public static const MOVE_CPU_RACKET:String = 'moveCpuRacket';
		public static const UPDATE_CPU_RACKET:String = 'updateCpuRacket';
		
		private var _racket:Sprite;
		
		public function Racket() 
		{
			
		}
		public function init(data:Object):void {
			_racket = new Sprite();
			_racket.graphics.lineStyle(1);
			_racket.graphics.beginFill(data.racketColor);
			_racket.graphics.drawRect(data.startX, data.startY, data.racketWidth, data.racketHeight);
			_racket.graphics.endFill();
			addChild(_racket);
		}
		
		public function get racketWidth():int {
			return _racket.width;
		}
		
		public function moveRacket(obj:Object):void {
			_racket.x = obj.posX - _racket.width/2;
		}
	}

}