package com.pingpong.view.component 
{
	import com.pingpong.model.vo.DataVO;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author 
	 */
	public class BallView extends Sprite 
	{
		public static const NAME:String = 'BallView';
		public static const UPDATE:String = NAME + 'Update';
		public static const MOVE_BALL:String = NAME + 'MoveBall';
		public static const CHANGE_DIRECTION:String = NAME + 'ChangeDirection';
		
		private var _ball:Sprite;
		
		public function BallView() 
		{
			
		}
		public function init(data:DataVO):void {
			_ball = new Sprite();
			_ball.graphics.lineStyle(1);
			_ball.graphics.beginFill(data.ballColor);
			_ball.graphics.drawCircle(0, 0, data.ballRadius);
			_ball.graphics.endFill();
			
			addChild(_ball);
		}
		
		public function setLoc(xLoc:int, yLoc:int):void {
			_ball.x = xLoc;
			_ball.y = yLoc;
		}
	}

}