package com.pingpong.view 
{
	import com.pingpong.model.CpuProxy;
	import com.pingpong.model.DataProxy;
	import com.pingpong.view.component.Racket;
	import flash.events.Event;
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	/**
	 * ...
	 * @author yangicher
	 */
	public class RacketMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'RacketMediator';
		
		private var _racket:Racket;
		private var _cpuRacket:Racket;
		
		public function RacketMediator(viewComponent:Object=null) 
		{
			super(NAME, viewComponent);
		}
		
		override public function onRegister():void 
		{
			_racket = new Racket();
			_racket.init(proxy.dataVO);
			viewComponent.addChild(_racket);
			
			_cpuRacket = new Racket();
			_cpuRacket.init((facade.retrieveProxy(CpuProxy.NAME) as CpuProxy).cpuVO);
			viewComponent.addChild(_cpuRacket);
		}
		
		override public function handleNotification(notification:INotification):void 
		{
			var name:String = notification.getName();
			var body:Object = notification.getBody();
			
			switch (name) {
				case Racket.MOVE_RACKET:
					_racket.moveRacket(body);
				break;
				case Racket.MOVE_CPU_RACKET:
					_cpuRacket.moveRacket(body);
				break;
			}
		}
		override public function listNotificationInterests():Array 
		{
			return [
				Racket.MOVE_RACKET,
				Racket.MOVE_CPU_RACKET
			];
		}
		private function get proxy():DataProxy {
			return facade.retrieveProxy(DataProxy.NAME) as DataProxy;
		}
	}

}