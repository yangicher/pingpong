package com.pingpong.view 
{
	import com.pingpong.view.component.Racket;
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	/**
	 * ...
	 * @author yangicher
	 */
	public class AppMediator extends Mediator implements IMediator
	{
		public static const NAME:String	= 'AppMediator';
		
		public function AppMediator(viewComponent:Object=null) 
		{
			super(NAME, viewComponent);
		}
		
		override public function onRegister():void 
		{
			sendNotification(Racket.GET_DATA);
		}
		override public function listNotificationInterests():Array
		{
			return [
				Racket.DATA_READY
			];
		}
		override public function handleNotification(notification:INotification):void 
		{
			var name:String = notification.getName();
			var body:Object = notification.getBody();
			
			switch (name) {
				case Racket.DATA_READY:
					facade.registerMediator(new RacketMediator(viewComponent));
					facade.registerMediator(new BallViewMediator(viewComponent));
					facade.registerMediator(new StageMediator(viewComponent));
				break;
			}
		}
	}

}