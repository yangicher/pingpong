package com.pingpong.view 
{
	import com.demonsters.debugger.MonsterDebugger;
	import com.pingpong.model.DataProxy;
	import com.pingpong.view.component.BallView;
	import flash.events.Event;
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	/**
	 * ...
	 * @author 
	 */
	public class BallViewMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'BallViewMediator';
		
		private var _ballView:BallView;
		
		public function BallViewMediator(viewComponent:Object=null) 
		{
			super(NAME, viewComponent);
		}
		override public function onRegister():void 
		{
			_ballView = new BallView();
			_ballView.init(proxy.dataVO);
			_ballView.setLoc(proxy.dataVO.ballX, proxy.dataVO.ballY);
			(viewComponent as Game).addChild(_ballView);
		}
		
		override public function handleNotification(notification:INotification):void 
		{
			var name:String = notification.getName();
			var body:Object = notification.getBody();
			
			switch (name) {
				case BallView.MOVE_BALL:
					_ballView.setLoc(body.posX, body.posY);
				break;
				//case BallView.CHANGE_DIRECTION:
					//_ballView.moveBall(proxy.dataVO.ballSpeed);
				//break;
			}
		}
		
		override public function listNotificationInterests():Array 
		{
			return [
				BallView.MOVE_BALL,
				BallView.CHANGE_DIRECTION
			];
		}
		private function get proxy():DataProxy {
			return facade.retrieveProxy(DataProxy.NAME) as DataProxy;
		}
	}

}