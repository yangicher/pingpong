package com.pingpong.view 
{
	import com.demonsters.debugger.MonsterDebugger;
	import com.pingpong.view.component.BallView;
	import com.pingpong.view.component.Racket;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.patterns.mediator.Mediator;
	/**
	 * ...
	 * @author 
	 */
	public class StageMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'StageMediator';
		
		public function StageMediator(viewComponent:Object = null) 
		{
			super(NAME, viewComponent);
		}
		override public function onRegister():void 
		{
			(viewComponent as Game).stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
			(viewComponent as Game).stage.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
		}
		
		private function onMouseMoveHandler(e:MouseEvent):void {
			sendNotification(Racket.MOUSE_MOVES, e.stageX);
		}
		
		private function onEnterFrameHandler(e:Event):void {
			sendNotification(BallView.UPDATE);
		}
	}

}