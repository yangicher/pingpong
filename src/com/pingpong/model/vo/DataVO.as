package com.pingpong.model.vo 
{
	/**
	 * ...
	 * @author yangicher
	 */
	public class DataVO 
	{
		//
		public var gameWidth:int = 400;
		public var gameHeight:int = 300;
		//
		public var score:int = 0;
		public var racketColor:uint = 0x0000FF;
		public var startX:int = 0;
		public var startY:int = 10;
		public var racketWidth:int = 100;
		public var racketHeight:int = 20;
		
		public var racketXposition:int = 0;
		
		//should be BallVO
		public var ballX:int = 150;
		public var ballY:int = 200;
		public var ballColor:uint = 0x008000;
		public var ballRadius:int = 10;
		public var ballSpeedX:int = 2;
		public var ballSpeedY:int = 2;
	}

}