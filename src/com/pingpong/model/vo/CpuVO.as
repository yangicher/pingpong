package com.pingpong.model.vo 
{
	/**
	 * ...
	 * @author 
	 */
	public class CpuVO 
	{
		public var gameWidth:int = 400;
		public var gameHeight:int = 300;
		
		public var racketColor:uint = 0xDB2432;
		public var startX:int = 0;
		public var startY:int = 260;
		public var racketWidth:int = 80;
		public var racketHeight:int = 20;
		
		public var racketXposition:int = 0;
	}

}