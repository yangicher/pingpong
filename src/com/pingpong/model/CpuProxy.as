package com.pingpong.model 
{
	import com.pingpong.model.vo.CpuVO;
	import com.pingpong.view.component.Racket;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	/**
	 * ...
	 * @author 
	 */
	public class CpuProxy extends Proxy implements IProxy
	{
		public static const NAME:String = 'CpuProxy';
		
		public function CpuProxy() 
		{
			super(NAME, new CpuVO());
		}
		
		public function get cpuVO():CpuVO {
			return data as CpuVO;
		}
		///
		
		public function set racketPosition(value:int):void {
			if (cpuVO.racketXposition != value) {
				cpuVO.racketXposition = value;
				sendNotification(Racket.MOVE_CPU_RACKET, { posX: value } );
			}
		}
	}

}