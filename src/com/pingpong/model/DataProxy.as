package com.pingpong.model 
{
	import com.pingpong.model.vo.DataVO;
	import com.pingpong.view.component.BallView;
	import com.pingpong.view.component.Racket;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * ...
	 * @author yangicher
	 */
	public class DataProxy extends Proxy implements IProxy 
	{
		public static const NAME:String = 'DataProxy';
		
		public function DataProxy() 
		{
			super(NAME, new DataVO());
		}
		
		public function loadData():void {
			trace('load data');
			sendNotification(Racket.DATA_READY);
		}
		
		public function get dataVO():DataVO {
			return data as DataVO;
		}
		
		public function set racketPosition(value:int):void {
			if (dataVO.racketXposition != value) {
				dataVO.racketXposition = value;
				sendNotification(Racket.MOVE_RACKET, { posX: value } );
			}
		}
		
		///should be BallProxy
		public function moveBall():void {
			dataVO.ballX += dataVO.ballSpeedX;
			dataVO.ballY += dataVO.ballSpeedY;
			sendNotification(BallView.MOVE_BALL, { posX: dataVO.ballX, posY: dataVO.ballY } );
			
			sendNotification(Racket.UPDATE_CPU_RACKET, dataVO.ballX);
		}
		public function set ballXPosition(value:int):void {
			dataVO.ballX = value;
		}
		public function set ballYPosition(value:int):void {
			dataVO.ballY = value;
		}
		public function changeBallSpeedX():void {
			dataVO.ballSpeedX *= -1;
		}
		public function changeBallSpeedY():void {
			dataVO.ballSpeedY *= -1;
		}
	}

}