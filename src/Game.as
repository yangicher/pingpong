package  
{
	import com.demonsters.debugger.MonsterDebugger;
	import com.pingpong.AppFacade;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author yangicher
	 */
	public class Game extends Sprite 
	{
		
		public function Game() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			//MonsterDebugger.initialize(this);
			AppFacade.getInstance().startUp(this);
			
		}
	}

}